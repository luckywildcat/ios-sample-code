//
//  SCPersistenceManager.m
//  iOS-Sample-Code
//
//  Persistence Manager - Singleton
//
//  Created by Carl von Havighorst on 4/19/16.
//  Copyright © 2016 Lucky Wild Cat Software. All rights reserved.
//

#import "SCPersistenceManager.h"

@implementation SCPersistenceManager

+ (SCPersistenceManager*) sharedInstance{
    static SCPersistenceManager*          globalInstance = nil;
    static dispatch_once_t		predicate;
    
    dispatch_once(&predicate, ^{
        globalInstance = [[self alloc] init];
    });
    
    return globalInstance;
}

@end
