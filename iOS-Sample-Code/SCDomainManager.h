//
//  SCDomainManager.h
//  iOS-Sample-Code
//
//  Created by Carl von Havighorst on 4/19/16.
//  Copyright © 2016 Lucky Wild Cat Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCDomainManager : NSObject

+ (SCDomainManager*) sharedInstance;

@end
